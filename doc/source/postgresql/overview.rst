PostgreSQL overview
===================


.. note::

    In this tutorial, the explanations of the commands are not given as the commands are discussed in "`MySQL Tutorials <http://mysqlguide.readthedocs.io/en/latest/>`_"


Databases is a collection of data, which can be created and updated using Structured Query Language (SQL) commands. The SQL commands are known as queries. There are various databases which support SQL e.g. MS-SQL, MySQL and PostgreSQL. PostgreSQL is the freely available "object-oriented relational database" system . We can write SQL queries in PostgreSQL to manage data. Further, there are non-relational base database e.g. Redis and Hbase etc. These databases are known as NoSQL databases. Also, there are databases which do not support SQL e.g. MongoDB. This page shows various SQL queries in PostgreSQL with examples.

.. note:: 

    * PostgreSQL commands and stored-data are case-insensitive in PostgreSQL.
    * But Database and table names are case-sensitive.
    * -- and /\* \*/ are used for comments.


Basic commands
--------------

This section includes the basic commands for creating a database.

Login and logout
^^^^^^^^^^^^^^^^

Execute following commands to enter into PostgreSQL server. Replace 'root' with correct username. Use 'exit' to quit from the mysql server.  

.. code-block:: shell
    

    (login : user 'postgres' is created during installation)
    $ sudo su - postgres
    [sudo] password for meher: 

    (start PostgreSQL session)
    postgres@meher:~$ psql
    psql (9.5.12)
    Type "help" for help.

    (exit PostgreSQL)
    postgres=# \q
    postgres@meher:~$ exit
    $ 


Display databases
^^^^^^^^^^^^^^^^^

'\\list' or '\\l' is used to see the complete list of databases, 

.. code-block:: shell

    postgres=# \list
                        List of databases              
       Name    |  Owner   | Encoding | ...    ...
    -----------+----------+----------+-...    ...
     postgres  | postgres | UTF8     | ...    ...
     template0 | postgres | UTF8     | ...    ...
     ... 


Create and delete database
^^^^^^^^^^^^^^^^^^^^^^^^^^


"CREATE DATABASE" and "DROP DATABASE" commands create and delete the database respectively. "testdb" is the name of the database defined by user. Further, each query (i.e. PostgreSQL statement) is ended with semicolon (;). 

.. code-block:: psql

    -- (create database)
    postgres=# CREATE DATABASE testdb;
    CREATE DATABASE

    -- (drop database)
    postgres=# DROP DATABASE testdb;
    DROP DATABASE



Select, connect and disconnect the database
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* '\\connect' or '\\c' command is used to select a database for operation. After connecting to the database, the user-name i.e. 'postgres' will be replaced by database-name as shown below, 

.. code-block:: psql

    postgres=# CREATE DATABASE writerdb;
    CREATE DATABASE

    postgres-# \c writerdb;
    You are now connected to database "writerdb" as user "postgres".
    writerdb-# 


* \\q is used for disconnecting from the database. Also, for deleting the database, we need to disconnect from it, as shown below, 

.. code-block:: psql

    writerdb-\# \q

    -- (connect again)
    postgres-# psql

    -- (delete database 'writerdb' if exists)
    postgres-# DROP DATABASE IF EXISTS writerdb;   

    -- (create and connect again)
    postgres-# CREATE DATABASE writerdb;
    postgres-# \c writerdb;
    writerdb-\#

Create and display Table
^^^^^^^^^^^^^^^^^^^^^^^^

.. note:: 

    writerdb-\# is removed from the queries for convenient reading,


.. code-block:: psql

    DROP TABLE IF EXISTS writer;   -- optional: delete the existed table
    CREATE TABLE writer
    (
      id    INT PRIMARY KEY NOT NULL,
      name  VARCHAR(30) NOT NULL UNIQUE,
      age   REAL
    );

    -- display tables 
    postgres=# \d
             List of relations
     Schema |  Name  | Type  |  Owner   
    --------+--------+-------+----------
     public | writer | table | postgres


    -- display columns of table 'writer'
    postgres=# \d writer;
               Table "public.writer"
     Column |         Type          | Modifiers 
    --------+-----------------------+-----------
     id     | integer               | not null
     name   | character varying(30) | not null
     age    | real                  | 
    Indexes:
        "writer_pkey" PRIMARY KEY, btree (id)
        "writer_name_key" UNIQUE CONSTRAINT, btree (name)



Insert data
^^^^^^^^^^^

.. code-block:: psql

    INSERT INTO writer VALUES
          (1, 'Rabindranath Tagore', 80),
          (2, 'Leo Tolstoy', 82);

    -- insert without age, as it can be NULL
    INSERT INTO writer (id, name) VALUES
          (3, 'Pearl Buck');

    -- insert one more data
    INSERT INTO writer (id, age, name) VALUES
          (4, 30, 'Meher Krishna Patel');


    -- display content of writer
    SELECT * FROM writer;

     id |        name         | age 
    ----+---------------------+-----
      1 | Rabindranath Tagore |  80
      2 | Leo Tolstoy         |  82
      4 | Meher Krishna Patel |  30
      3 | Pearl Buck          |    
    (4 rows)


Select statement
----------------

In previous section, SELECT query which display the complete table. In general, SELECT statement is used with various other clauses e.g. WHERE, LIKE and ORDER BY etc. to specify the selection criteria. In this section, various such clauses are shown. 

Basic SELECT Queries
^^^^^^^^^^^^^^^^^^^^

*  SELECT colName1, colName2, ... FROM tableName ; 

.. code-block:: psql

    SELECT name, age FROM writer;

            name         | age 
    ---------------------+-----
     Rabindranath Tagore |  80
     Leo Tolstoy         |  82
     Meher Krishna Patel |  30
     Pearl Buck          |    
    (4 rows)



Where
^^^^^

* SELECT colName1, colName2, ... FROM tableName WHERE conditions ;  

.. code-block:: psql

    -- Select Leo Tolstoy only
    SELECT * FROM writer WHERE name = 'Leo Tolstoy'; 

     id |    name     | age 
    ----+-------------+-----
      2 | Leo Tolstoy |  82
    (1 row)

    -- Selects id between 1 and 4 i.e 2 and 3
    SELECT * FROM writer WHERE (id >1 and id < 4);

      id |    name     | age 
     ----+-------------+-----
       2 | Leo Tolstoy |  82
       3 | Pearl Buck  |    
     (2 rows)

    

.. note:: We can not use 1< id < 4 in SQL. We have to use 'and/or' for multiple conditions.


Inserting data with SELECT-WHERE
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Before moving further, lets create another table "book", which stores the names of the books by the authors in table 'writer'.

* In the below table, SELECT-WHERE statements are used to insert the writer_id into 'book' from 'writer' table. 

.. warning:: 

  * In table 'book', 'SERIAL' is used for 'book_id' which is equivalent to 'auto increment' in MySQL. 
  * But, unlike MySQL, PostgreSQL will generate error for 'automatic increment' if we define 'book_id' manually at some places. To avoid errors, do not define 'SERIAL' manually. 

.. code-block:: psql

  DROP TABLE IF EXISTS book;
  CREATE TABLE book -- creating table 'book'
      (
       writer_id  INT NOT NULL,               
       book_id  SERIAL NOT NULL PRIMARY KEY,
       title CHAR(50) NOT NULL,                
       price INT         
      );


  -- Insert data in the table
  INSERT INTO book (writer_id,title,price)
    SELECT id, 'The Good Earth', 200         -- select id
    FROM writer WHERE name = 'Pearl Buck';   -- where name = 'Pearl Buck'

  INSERT INTO book (writer_id,title,price)
    SELECT id, 'The Home and The World',  250
    FROM writer WHERE name = 'Rabindranath Tagore';

  INSERT INTO book (writer_id,title,price)
    SELECT id, 'Gitanjali', 100
    FROM writer WHERE name = 'Rabindranath Tagore';

  INSERT INTO book (writer_id,title,price)
      SELECT id, 'War and Peace', 200
      FROM writer WHERE name = 'Leo Tolstoy';

  INSERT INTO book (writer_id,title,price)
      SELECT id, 'Anna Karenina', 100
      FROM writer WHERE name = 'Leo Tolstoy';


* Following is the data stored in 'book' table, 

.. code-block:: psql

  SELECT * FROM book;

   writer_id | book_id |                       title                        | price 
  -----------+---------+----------------------------------------------------+-------
           3 |       1 | The Good Earth                                     |   200
           1 |       2 | The Home and The World                             |   250
           1 |       3 | Gitanjali                                          |   100
           2 |       4 | War and Peace                                      |   200
           2 |       5 | Anna Karenina                                      |   100
  (5 rows)


ORDER BY
^^^^^^^^

* Syntax: 

.. code-block:: psql

    SELECT colName1, colName2, ... FROM tableName 
        ORDER BY 'colName1' ASC, colName2 DESC, ... ;

* Example:

.. code-block:: psql

   -- ORDER BY: ASC by default
   SELECT title, price FROM book ORDER BY title; 

                          title                        | price 
   ----------------------------------------------------+-------
    Anna Karenina                                      |   100
    Gitanjali                                          |   100
    The Good Earth                                     |   200
    The Home and The World                             |   250
    War and Peace                                      |   200
   (5 rows)


   -- descending order
   SELECT title, price FROM book ORDER BY title DESC;

                          title                        | price 
   ----------------------------------------------------+-------
    War and Peace                                      |   200
    The Home and The World                             |   250
    The Good Earth                                     |   200
    Gitanjali                                          |   100
    Anna Karenina                                      |   100
   (5 rows)


   
   -- First arrange by price and then by title.
   -- 'The Good Earth' and 'War and Peace' have same prices.
   -- Since 'title ASC' is used, therefore 'The Good Earth' is place above the 'War and Peace'.

                          title                        | price 
   ----------------------------------------------------+-------
    War and Peace                                      |   200
    The Home and The World                             |   250
    The Good Earth                                     |   200
    Gitanjali                                          |   100
    Anna Karenina                                      |   100
   (5 rows)



LIKE
^^^^

LIKE is used for pattern matching with percentage ( \% ) and underscore ( \_ ) signs.

.. note:: 

  Unlike MySQL, The 'LIKE' statments are case-sensitive i.e. 'th' and 'Th' will be treated differently. 

.. code-block:: psql

    -- %Th: find titles which start with 'Th'.
    SELECT title, price FROM book WHERE title LIKE 'Th%';

                           title                        | price 
    ----------------------------------------------------+-------
     The Good Earth                                     |   200
     The Home and The World                             |   250
    (2 rows)
    2 rows in set (0.00 sec)

    -- %an%: find titles which contain 'an'. 
    SELECT title, price FROM book WHERE  title LIKE '%an%';
    
                           title                        | price 
    ----------------------------------------------------+-------
     The Home and The World                             |   250
     Gitanjali                                          |   100
     War and Peace                                      |   200
    (3 rows)

.. warning:: note

  Unlike MySQL, we can not search the book which ends with 'th' using '%th'; we must use 
  '\:\:text' as shown below, 


  .. code-block:: psql
  
    -- %th: find titles which end with 'th'.
    SELECT title, price FROM book WHERE title::text LIKE '%th';

                           title                        | price 
    ----------------------------------------------------+-------
     The Good Earth                                     |   200


    -- %_Th%: find titles which contains atleast one letter before Th
    SELECT title, price FROM book WHERE title LIKE '%_Th%';

                           title                        | price 
    ----------------------------------------------------+-------
     The Home and The World                             |   250


.. note::  

    % : it looks for zero or more characters to fill it's place
    _ : It looks for exactly one character to fill it's place. For two characters, use two underscore and so on.


* Try these commands as well, 

.. code-block:: psql

    -- Try for these commands also.
    SELECT * FROM book WHERE title::text LIKE '_rt_';

    SELECT * FROM book WHERE title::text LIKE '%rt_';  

    -- two underscore arter 'rt'
    SELECT * FROM book WHERE title::text LIKE '%rt__';

    -- three underscore after 'rt'
    SELECT * FROM book WHERE title::text LIKE '%r___';        

    SELECT * FROM book WHERE title::text LIKE '%r_%';
